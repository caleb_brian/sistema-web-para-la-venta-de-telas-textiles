import axios from 'axios'

export default {
    namespaced:true,
    state:{
        allposts:[],
        allpostsuser:[],
        detailpost:{}
    },
    getters:{
        responsePosts(state){
            return state.allposts
        },
        getallpostsuser(state){
            return state.allpostsuser
        },
        detailposts(state){
            return state.detailpost
        }
    },
    mutations:{
        SET_ALL_POSTS(state,value){
            state.allposts = value
        },
        SET_ALL_POSTS_USER(state,value){
            state.allpostsuser = value
        },
        SET_DETAIL_ALL_POSTS(state,value){
            state.detailpost = value
        }
    },
    actions:{
        async getAllPost({dispatch}){
            console.log('requiero las publicaciones')
            await axios.get('/sanctum/csrf-cookie')
            let gp = await axios.get('/api/posts')
            const data={
                type:'allpost',
                data:gp.data
            }
            return dispatch('mePosts',data)
        },
        async createPost({dispatch},data){
            console.log('enviando imfo publicacion')
            console.log(data)
            await axios.get('/sanctum/csrf-cookie')
            await axios.post('/api/posts',data.formdata,data.config)
            ///return dispatch('mePosts',gp.data)
        },
        async getAllPostUser({dispatch}){
            console.log('Solicito una Publicacion del Usuario')
            await axios.get('/sanctum/csrf-cookie')
            let gpu = await axios.get('/api/post/user-posts')
            console.log(gpu)
            const data = {
                type:'postuser',
                data:gpu.data
            }
            return dispatch('mePosts',data)
        },
        async detailPost({dispatch}, parametro){
            console.log('El detalle de la Publicacion')
            console.log(parametro)
            await axios.get('/sanctum/csrf-cookie')
            let par = await axios.get('/api/posts/'+parametro)
            const data = {
                type:'detailpost',
                data:par.data
            }
            return dispatch('mePosts',data)
        },
        async editPost({dispatch},data){
            console.log(data)
            console.log('actualizacion publicacion')
            await axios.get('/sanctum/csrf-cookie')
            await axios.post(`/api/posts/${data.param}`,data.formdata,data.config)
            //let edit = await axios.post('/api/post/update-my-post',data.formdata,data.config)
        },
        async deletePost({dispatch},paramet){
            console.log(paramet);
            console.log('eliminar publicacion')
            await axios.get('/sanctum/csrf-cookie')
            await axios.delete(`/api/posts/${paramet}`).then((response)=>{
                dispatch('getAllPostUser')
            }).catch((error)=>{
                alert(error)
            })
        },
            mePosts({commit},data){
            //console.log(getAllPost)
            //commit('SET_ALL_POSTS',getAllPost)

            switch (data.type) {
                case 'postuser':
                    commit('SET_ALL_POSTS_USER',data.data)
                    break;
                case 'allpost':
                    commit('SET_ALL_POSTS',data.data)
                    break;
                case 'detailpost':
                    console.log(data.data)
                    commit('SET_DETAIL_ALL_POSTS',data.data)
                    break;
                default:
                    break;
            }
        }
    }
}
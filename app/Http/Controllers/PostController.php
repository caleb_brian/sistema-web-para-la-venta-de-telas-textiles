<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
///
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Carbon::setlocale('es');
        $posts = Post::latest()->paginate(12);
        foreach($posts as $post){
            $post->setAttribute('user',$post->user);
            $post->setAttribute('added',Carbon::parse($post->created_at)->diffForHumans());
            $post->setAttribute('path',$post->slug);
        }
        return response()->json($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //dd($request->all());
        if($request['file']){
            $imageNom = time().'.'.$request['file']->getClientOriginalExtension();
            //dd($request->all());
            $data = json_decode($request['form']);
            //dd($data->title);
            Post::create([
                'title' => $data->title,
                'slug' => Str::slug($data->title),
                'body' => $data->body,
                'precio' => $data->precio,
                'textura' => $data->textura,
                'category_id' => $data->category,
                'user_id' => Auth::user()->id,
                'photo' => '/uploads/'.$imageNom
            ]);
            $request['file']->move(public_path('uploads'),$imageNom);
    
            return response()->json(['state'=>'creacion de la publicacion'], 200);
        }else{
            return response()->json(['state'=>'no existe la imggen'], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //dd('la solicitud del detalle de la publicacion');
        Carbon::setlocale('es');
        return response()->json([
            'id'=>$post->id,
            'title'=>$post->title,
            'body'=>$post->body,
            'photo'=>$post->photo,
            'created_at'=>$post->created_at->diffForHumans(),
            'user'=>$post->user->name,
            'category'=>$post->category->name,
            'categoryid'=>$post->category->id,
            'comments_tario'=>$post->comments->count(),
            'comments'=> $this->allComments($post->comments)
        ]);
    }

    public function allComments($comments){
        $jsonTario = [];
        foreach($comments as $comments){
            array_push($jsonTario,[
                'id'=>$comments->id,
                'body'=>$comments->body,
                'created'=>$comments->created_co,//->diffForHumans(),
                'user'=>$comments->user->name,
            ]);
        }
        return $jsonTario;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //dd($request->all());
        //dd('este es el metodo para actualizar por el metodo por defecto')
        //dd($request->all());
        if(Auth::user()->id == $post->user_id){
            if($request['file']){
                $imageNom = time().'.'.$request['file']->getClientOriginalExtension();
                $request['file']->move(public_path('uploads'),$imageNom);
                $post->photo = '/uploads/'.$imageNom;
            }
            
                $form = json_decode($request['form']);
                $post->title = $form->title;
                $post->slug = Str::slug($form->title);
                $post->precio =  $form->precio;
                $post->textura = $form->textura;
                $post->category_id = $form->category;
                $post->body = $form->body;
            
            $post->save();
            return response()->json(['state'=>'eeditado con exito'], 200);
        }
        else{
            return response()->json(['state'=>'editacion error'], 401);
        }
    }

    public function updateMyPost(Request $request)
    {
        //dd('aqui tambien voy actualizar');
        //dd($request->all());
        if(Auth::user()->id == $post->user_id){
            $post->delete();
            return response()->json(['state'=>'eliminado con exito'], 200);
        }else{
            return response()->json(['state'=>'editacion error'], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //dd('aqui eliminar');
        $post->delete();
        return response()->json(['state'=>'eliminado con exito'], 200);
    }
    public function allPostForUser(){
        Carbon::setlocale('es');
        $posts = Post::latest()->where('user_id',Auth::user()->id)->paginate(8);
        foreach($posts as $post){
            $post->setAttribute('user',$post->user);
            $post->setAttribute('added',Carbon::parse($post->created_at)->diffForHumans());
            $post->setAttribute('path','/post/'.$post->slug);
        }
        return response()->json($posts);        
    }
}
